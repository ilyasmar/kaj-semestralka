# audio player

## 1. introduce
html5 the audio player(with vizualization and playlist) using flexbox, canvas, css animations, js api and libraries: ID3, jquery.

## 2. Information
An audio player was made with the ability to play, pause, stop, rewind the audio file, and switch to the previous and next track. Navigating and launching songs through the playlist.

A range is available for rewinding and adjusting the sound, random ordering of songs (shuffle), repeating and adding new songs to the playlist. These functions are available after pressing the "additional actions" button located on the left side.

Also made an audio visualization, which is turned on and off by pressing the button in the form of an eye, located in the center of the screen. When you turn on the visualization, the rewind and pause buttons will be disabled, because the AudioBufferSourceNode does not support this functionality and will require a redefine.  Songs in this mode will be launched with a delay, which is necessary for loading the buffer and decoding.


class audioVizualizer {
    constructor(){
        this.analyser = null;
        this.cnvs = null;
        this.ctx = null;
        this.fileChosen = false;
        this.sourceNode = null;
        this.context = new AudioContext() ||webkitAudioContext || mozAudioContext;

        if (!window.requestAnimationFrame)
            window.requestAnimationFrame = window.webkitRequestAnimationFrame;
    }


    vizualize(url) {
        this.fileChosen = true;
        this.setupAudioNodes();
        var request = new XMLHttpRequest();

        request.addEventListener("progress", $("button, input").prop("disabled",true));
        request.addEventListener("load", $("button, input").prop("disabled",false));

        request.open('GET', url, true);
        request.responseType = 'arraybuffer';
        // When loaded decode the data
        request.onload = function () {
            // decode the data
            this.context.decodeAudioData(request.response, function (buffer) {
                // when the audio is decoded play the sound
                this.sourceNode.buffer = buffer;
                this.sourceNode.start(0);
                console.log(this.sourceNode.state);
                //on error
            }.bind(this), function (e) {
                console.log(e);
            });
        }.bind(this);
        request.send();

        //disable some buttons in vizual mode
        $("button[id=forward-button]").prop("disabled", true);
        $("button[id=backward-button]").prop("disabled", true);
        $("button[id=pause-button]").prop("disabled", true);
        $("input[id=songTimeRange]").prop("disabled", true);
        $("input[id=volumeLevel]").prop("disabled", true);
    }

    enableButtons() {
        $("button, input").prop("disabled",false);
        this.fileChosen = false;
    }

    initCanvas() {

        this.cnvs = document.getElementById("canvas");
        this.cnvs.width = window.innerWidth;
        this.cnvs.height = window.innerHeight;
        //get this.context from canvas for drawing
        this.ctx = this.cnvs.getContext("2d");

        this.ctx.canvas.width = window.innerWidth;
        this.ctx.canvas.height = window.innerHeight;

        window.addEventListener('resize', this.onWindowResize.bind(this), false);
    }

    setupAudioNodes() {
        // setup a analyser
        this.analyser = this.context.createAnalyser();
        // create a buffer source
        this.sourceNode = this.context.createBufferSource();

        this.sourceNode.connect(this.analyser);
        this.sourceNode.connect(this.context.destination);

        //start updating
        window.requestAnimationFrame(this.updateVisualization.bind(this));
    }

    onWindowResize() {
        this.ctx.canvas.width = window.innerWidth;
        this.ctx.canvas.height = window.innerHeight;
    }

    drawBars(array) {
        var threshold = 0;
        // clear the current state
        this.ctx.clearRect(0, 0, this.cnvs.width, this.cnvs.height);
        //the max count of bins for the visualization
        var maxBinCount = array.length;

        this.ctx.save();

        this.ctx.globalCompositeOperation = 'source-over';

        this.ctx.scale(0.5, 0.26);
        this.ctx.translate(window.innerWidth, window.innerHeight*0.90);
        //bins color
        this.ctx.fillStyle = "#220203";

        var bass = Math.floor(array[1]);
        var radius = 0.45 * $(window).width() <= 450 ? -(bass * 0.25 + 0.20 * $(window).width()) : -(bass * 0.25 + 200);

        var bar_length_factor = 1;
        if ($(window).width() >= 785) {
            bar_length_factor = 1;
        } else if ($(window).width() < 785) {
            bar_length_factor = 1.5;
            this.ctx.translate(0, -100);
        } else if ($(window).width() < 500) {
            bar_length_factor = 50.0;
        }

        //go over each bin
        for (var i = 0; i < maxBinCount; i+= 2) {

            var value = array[i];
            if (value >= threshold) {
                this.ctx.fillRect(0, radius, $(window).width() <= 450 ? 2 : 3, -value / bar_length_factor);
                this.ctx.rotate((180 / 128) * Math.PI / 180);
            }
        }

        for (var i = 0; i < maxBinCount; i+= 2) {

            var value = array[i];
            if (value >= threshold) {
                this.ctx.rotate(-(180 / 128) * Math.PI / 180);
                this.ctx.fillRect(0, radius, $(window).width() <= 450 ? 2 : 3, -value / bar_length_factor);
            }
        }

        for (var i = 0; i < maxBinCount; i+= 2) {

            var value = array[i];
            if (value >= threshold) {
                this.ctx.rotate((180 / 128) * Math.PI / 180);
                this.ctx.fillRect(0, radius, $(window).width() <= 450 ? 2 : 3, -value / bar_length_factor);
            }
        }

        this.ctx.restore();
    }

    updateVisualization() {
        if (this.fileChosen) {
            console.log(this.context.currentTime);
            var array = new Uint8Array(this.analyser.frequencyBinCount);
            this.analyser.getByteFrequencyData(array);
            this.drawBars(array);
        }

        window.requestAnimationFrame(this.updateVisualization.bind(this));
    }
}


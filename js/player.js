class audioPlayer {
  constructor(){
    this.song = document.getElementById("currentSong");
    this.songTime = document.getElementById("currentSongTotalTime");
    this.songTimeRange = document.getElementById("songTimeRange");
    this.randomSong = false;
    this.currentSong = 0;
    this.audio = document.getElementById("song");
    this.noAlbumArt = "img/no_album_art.jpg";
    this.vizualizer = new audioVizualizer();
    this.isVisual = false;
    this.songs = [
      [
        "songs/star-wars-theme-song.mp3",
        "John Williams",
        "Star Wars theme",
        "img/starwars-theme.jpg"
      ],
      [
        "songs/imperial_march.mp3",
        "Darth Vader",
        "Imperial march",
        "img/starwars-imperial-march.jpg"
      ],
      [
        "songs/dark-knight-rises.mp3",
        "Hans Zimmer",
        "The Dark Knight rises",
        "img/dark-knight-rises-cover.jpg"
      ],
      [
        "songs/pirates-of-the-caribbean.mp3",
        "Hans Zimmer",
        "Pirates of the Caribbean",
        "img/pirates-of-the-caribbean-cover.jpg"
      ]

    ];

    for (let [index, value] of this.songs.entries()) {
      document.getElementById("song-list").innerHTML += `<li class="song-list__item" data-index="${index}">${value[2]}</li>`;
    }

  }

  openButtonBars() {
    if (!this.isVisual) {
      this.play();
    }
      const hiddenButtons = document.querySelectorAll(".button-bar--primary > button");
      for (let i = 0; i < hiddenButtons.length; i++) {
        hiddenButtons[i].classList.add("is-shown");
      }
      setTimeout(() => {
        document.getElementById("songdata").classList.add("is-visible");
      }, 3000);

  }


  switchPlayPause() {
    document.getElementsByClassName("btn--play")[1].classList.toggle("is-visible");
  }

  switchVizualizeButtons() {
    document.getElementsByClassName("btn--viz")[1].classList.toggle("is-visible");
  }

  play() {
    if ((!this.audio.src && this.randomSong == true) || (this.audio.src && this.randomSong == true)) {
      this.shuffleSongs();
    } else if (this.audio.src) {

      const playPromise = this.audio.play();
      if (playPromise !== null){
        playPromise.catch((error) => { console.log(error) })
      }

    } else {
      this.audio.src = this.songs[this.currentSong][0];
    }

    if (this.isVisual) {
      this.stopVizual()
      this.playVizual();
    }
    else {
      this.audio.play();
    }

    //if is a local music file change src
    var localSrc = this.audio.src.substr(this.audio.src.indexOf("songs"));

    let songIndexinArr = this.findIndexInNestedArray(this.audio.src, this.songs)
        || this.findIndexInNestedArray(localSrc, this.songs);

    this.song.textContent = this.songs[songIndexinArr][2];
    document.getElementById("currentSongAuthor").textContent = this.songs[songIndexinArr][1];
    document.getElementById("currentSongCover").setAttribute("src", this.songs[songIndexinArr][3]);

    this.convertDuration(this.song.duration);
    this.switchPlayPause();
  }

  playSong(s) {
    this.currentSong = s;
    this.audio.src = this.songs[this.currentSong][0];
    this.play();
  }

  playVizual() {
    this.openButtonBars();

    this.stop();
    this.isVisual = true;

    if (!this.audio.src) {
      this.audio.src = this.songs[0][0];
    }
    this.vizualizer.vizualize(this.audio.src);
    this.switchVizualizeButtons();
  }

  stopVizual() {
    this.isVisual = false;
    this.vizualizer.enableButtons();

    this.vizualizer.sourceNode.stop();
    this.vizualizer.sourceNode.disconnect();
    this.vizualizer.sourceNode.stop(0);
    this.vizualizer.sourceNode = null;

    this.switchVizualizeButtons();
  }

  pause() {
    this.audio.pause();
  }

  stop() {
    console.log(this.audio.currentTime)
    if (this.audio.currentTime > 0 && !this.audio.paused) {
      this.switchPlayPause();
    }

    if (this.isVisual) {
      this.stopVizual();
    }
    else {
      this.audio.pause();
      this.audio.currentTime = 0;
    }
  }

  forward() {
    /*rewind for source node, commented  out coz of inefficiency
    if(this.isVisual){
      this.vizualizer.sourceNode.stop();
      this.vizualizer.vizualize(this.audio.src, this.vizualizer.context.currentTime, 10);
    }*/
    console.log(this.audio.currentTime);
    this.audio.currentTime += 10;
  }

  backward() {
      this.audio.currentTime -= 10;
  }

  stepForward() {
    this.currentSong++;
    if (this.currentSong == this.songs.length) {
      this.currentSong = 0;
    }
    this.audio.src = this.songs[this.currentSong][0];
    this.play();
  }

  stepBackward() {
    --this.currentSong;
    if (this.currentSong < 0) {
      this.currentSong = this.songs.length - 1;
    }
    this.audio.src = this.songs[this.currentSong][0];
    this.play();
  }

  toggleRandom() {
    this.randomSong = !this.randomSong;
    return this.randomSong;
  }

  toggleRepeat() {
    this.audio.loop = !this.audio.loop;
    return this.audio.loop;
  }

  shuffleSongs() {
    if (this.randomSong == true) {
      this.audio.src = this.songs[Math.floor(Math.random() * this.songs.length)][0];
    }
  }

  //cycle in another function for ID3.LoadTags, which doesn't work correctly in another way
  addSongs(files) {
    let index = this.songs.length;
    for (var i = 0; i < files.length; i++) {
      this.addSong(files[i], index);
      index ++;
    }
  }

  addSong(file, index) {
      let url = URL.createObjectURL(file);
      ID3.loadTags(url, function () {
        let tags = ID3.getAllTags(url);
        this.songs[index] = [url, tags.artist, tags.title, this.noAlbumArt]
        document.getElementById("song-list").innerHTML += `<li class="song-list__item" data-index="${index}">${this.songs[index][2]}</li>`;

      }.bind(this), {
        tags: ["title", "artist", "album", "picture"],
        dataReader: ID3.FileAPIReader(file)
      });
  }

  convertDuration(duration) {
    let minutes = Math.floor(duration / 60);
    let seconds = Math.floor(duration % 60);
    if (seconds < 10) {
      seconds = `0${seconds}`;
    }
    return `${minutes}:${seconds}`;
  }

  changeVolume(amount) {
    this.audio.volume = amount;
  }

  changeSongProgress(value) {
    this.audio.currentTime = value;
  }

  applyActiveClass(el) {
    el.classList.toggle("is-active");
  }

  findIndexInNestedArray(str, arr) {
    for (let i = 0; i < arr.length; i++) {
        if (arr[i][0] === str) {
          return [i][0];
        }
    }
    return false;
  }

  setListeners() {
    const clickHandlers = {

      "stop-button" : this.stop.bind(this),

      "backward-button" : this.backward.bind(this),

      "stepBackward-button" : this.stepBackward.bind(this),

      "forward-button" : this.forward.bind(this),

      "stepForward-button" : this.stepForward.bind(this),

      "viz-button"  : this.playVizual.bind(this),

      "noviz-button"  : this.stopVizual.bind(this),

      "more-controls" :  (e) => {
        this.applyActiveClass(e.currentTarget);
        document.getElementsByClassName("button-bar--secondary")[0].classList.toggle("is-visible");
      },

      "random-song" : (e) => {
        this.applyActiveClass(e.currentTarget);
        this.toggleRandom();
      },

      "repeat-song" : (e) => {
        this.applyActiveClass(e.currentTarget);
        this.toggleRepeat();
      },

      "song-list" : (e) => {
        if (e.target && e.target.nodeName == "LI") {
          this.playSong(e.target.dataset.index);
        }
      },

      "play-button" : this.openButtonBars.bind(this),

      "pause-button" : () => {
        this.pause();
        this.switchPlayPause();
      }
    };

    Object.keys(clickHandlers).forEach(function(key) {
      document.getElementById(key).addEventListener("click", clickHandlers[key]);
    });

    document.getElementById("volumeLevel").addEventListener("change", (e) => {
      this.changeVolume(e.currentTarget.value);
    });

    document.getElementById("songTimeRange").addEventListener("change", (e) => {
      this.changeSongProgress(e.currentTarget.value);
    });

    this.audio.addEventListener("ended", () => {
      this.currentTime = 0;
      this.stepForward();
    });

    this.audio.addEventListener("timeupdate", () => {
      document.getElementById("currentSongCurrentTime").innerHTML = this.convertDuration(this.audio.currentTime) + " / ";

      this.songTimeRange.min = this.audio.startTime;
      this.songTimeRange.max = this.audio.duration;
      this.songTimeRange.value = this.audio.currentTime;

    });

    this.audio.addEventListener("loadedmetadata", () => {
      this.songTime.innerHTML = this.convertDuration(this.audio.duration);
    });

  }

}

var player = new audioPlayer();
player.setListeners();

$(function () {
  player.vizualizer.initCanvas();
});

